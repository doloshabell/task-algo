const binarySearch = (nums, target, start = 0, end = nums.length - 1) => {
    if (start > end) {
        return -1
    }
    let mid = Math.floor((start + end) / 2)

    if (nums[mid] === target) {
        return mid
    }

    if (nums[mid] < target) {
        return target + " berada di index " + binarySearch(nums, target, mid + 1, end)
    } else {
        return target + " berada di index " + binarySearch(nums, target, mid - 1)
    }
}

console.log(binarySearch([1, 6, 6, 7, 4, 8, 3, 9], 8))