#include <stdio.h>

int main()
{
    int choice;
    float num;
    float temp = 0;
    float result = 0;
    
    printf("Welcome to Basic Calculator \n");
    printf("Input Number : ");
    scanf("%f",&temp);
    printf("%0.2f", temp);
    menu();
    
    printf("Input Your Choice : ");
    scanf("%d",&choice);
    
    
    while(choice != 5) {
        switch (choice) {
            case 1: 
            printf("You choose 1. + \n");
            printf("Input other number: ");
            scanf("%f",&num);
            printf("%0.2f + %0.2f \n", temp, num);
            result = temp + num;
            temp = result;
            break;
            
            case 2: 
            printf("You choose 2. - \n");
            printf("Input other number: ");
            scanf("%f",&num);
            printf("%0.2f - %0.2f \n", temp, num);
            result = temp - num;
            temp = result;
            break;
            
            case 3: 
            printf("You choose 3. x \n");
            printf("Input other number: ");
            scanf("%f",&num);
            printf("%0.2f x %0.2f \n", temp, num);
            result = temp * num;
            temp = result;
            break;
            
            case 4: 
            printf("You choose 4. / \n");
            printf("Input other number: ");
            scanf("%f",&num);
            printf("%0.2f / %0.2f \n", temp, num);
            result = temp / num;
            temp = result;
            break;
        }
        menu();
        printf("Input Your Choice : ");
        scanf("%d",&choice);
    }
    
    printf("result: %0.2f", result);

    return 0;
}

void menu() {
    printf("Choice What You Want Todo\n ");
    printf("1. +\n ");
    printf("2. -\n ");
    printf("3. x\n ");
    printf("4. /\n ");
    printf("5. Get Result\n ");
}